<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\UsersJobOffer;
use App\Entity\UsersJobOffersStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class UsersJobOffersStatusFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
         
        for ($i = 0; $i < 20; $i++) {
            $newUsersJobOffersStatus = new UsersJobOffersStatus();
            
            $newUsersJobOffersStatus->setUser($this->getReference(User::class.'_'.rand(0, 9)));
            $newUsersJobOffersStatus->setUsersJobOffer($this->getReference(UsersJobOffer::class.'_'.$i));

            $status = $faker->randomElement($array = ['Accepted', 'Pending', 'Refused']);
            $newUsersJobOffersStatus->setStatus($status);

            if ($status != 'Pending') $newUsersJobOffersStatus->setComment($faker->optional()->sentences($nb = 3, $asText = true));
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newUsersJobOffersStatus->setCreatedAt($datetime);
            $newUsersJobOffersStatus->setUpdatedAt($datetime);

            $manager->persist($newUsersJobOffersStatus);
            $this->addReference(UsersJobOffersStatus::class.'_'.$i, $newUsersJobOffersStatus);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, UsersJobOfferFixtures::class];
    }
}