<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\Company;
use App\Entity\CompanyRate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CompanyRateFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
         
        for ($i = 0; $i < 20; $i++) {
            $newCompanyRate = new CompanyRate();

            $newCompanyRate->setRate($faker->randomElement($array = [0, 1, 2, 3, 4, 5]));
            $newCompanyRate->setComment($faker->optional()->sentences($nb = 3, $asText = true));
            
            $newCompanyRate->setUser($this->getReference(User::class.'_'.rand(0, 9)));
            $newCompanyRate->setCompany($this->getReference(Company::class.'_'.rand(0, 19)));

            $manager->persist($newCompanyRate);
            $this->addReference(CompanyRate::class.'_'.$i, $newCompanyRate);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, CompanyFixtures::class];
    }
}