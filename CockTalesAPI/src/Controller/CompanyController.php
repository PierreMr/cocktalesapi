<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Models\CompanyModel;
use App\Repository\CompanyTypeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company")
 */
class CompanyController extends AbstractController
{

	private $companyRepository;
    private $serializer;
    private $companyModel;
    private $companyTypeRepository;
    private $userTypeRepository;
        
	public function __construct(
        CompanyRepository $companyRepository,
        SerializerInterface $serializer,
        CompanyModel $companyModel,
        CompanyTypeRepository $companyTypeRepository,
        UserRepository $userRepository
    )
    {
		$this->companyRepository = $companyRepository;
		$this->serializer = $serializer;
		$this->companyModel = $companyModel;
        $this->companyTypeRepository = $companyTypeRepository;
        $this->userRepository = $userRepository;
    }
    
    /**
     * @Route("/", name="company_index", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $companies = $this->companyRepository->findAll();

        $serialize = $this->serializer->serialize($companies, 'json', ['groups' => 'company']);
        $results = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($results);
    }

    /**
     * @Route("/new", name="company_new", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {
        $body = $request->getContent();

        $companyModel = $this->serializer->deserialize($body, CompanyModel::class, 'json');
        $companyType = $this->companyTypeRepository->find($companyModel->getCompanyType());
        $user = $this->userRepository->find($companyModel->getUser());

        $company = new Company();

        $company->setName($companyModel->getName());
        $company->setAddress($companyModel->getAddress());
        $company->setZip($companyModel->getZip());
        $company->setCity($companyModel->getCity());
        $company->setLat($companyModel->getLat());
        $company->setLng($companyModel->getLng());
        $company->setTheme($companyModel->getTheme());
        $company->setPhone($companyModel->getPhone());
        $company->setEmail($companyModel->getEmail());
        $company->setWebsite($companyModel->getWebsite());
        $company->setDescription($companyModel->getDescription());
        $company->setImage($companyModel->getImage());
        $company->setCreatedAt($companyModel->getCreatedAt());
        $company->setUpdatedAt($companyModel->getUpdatedAt());
        $company->setCompanyType($companyType);
        $company->addUser($user);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($company, 'json', ['groups' => 'company']));
    }

    /**
     * @Route("/{id}", name="company_show", methods={"GET"})
     */
    public function show(Company $company = null): JsonResponse
    {   
        if (!$company) {
            return new JsonResponse(["message" => "Invalid Company number"]);
        }

        $serialize = $this->serializer->serialize($company, 'json', ['groups' => 'company']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/edit", name="company_edit", methods={"PUT"})
     */
    public function edit(Request $request, Company $company): JsonResponse
    {
        $body = $this->serializer->decode($request->getContent(), 'json');
        
        $company->updateCompany($company, $body);

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);

        $company->setUpdatedAt($datetime);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($company, 'json', ['groups' => 'company']));
    }

    /**
     * @Route("/{id}", name="company_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Company $company): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($company);
        $entityManager->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/user/{id}", name="company_user_list", methods={"GET"})
     */
    public function user(User $user = null): JsonResponse
    {   
        if (!$user) {
            return new JsonResponse(["message" => "Invalid user number"]);
        }

        $companies = $user->getCompanies();

        $serialize = $this->serializer->serialize($companies, 'json', ['groups' => 'company']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }
}
