<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $read;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="notifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsersJobOffer", inversedBy="notifications")
     */
    private $usersJobOffer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsersJobOffersStatus", inversedBy="notifications")
     */
    private $usersJobOffersStatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(bool $read): self
    {
        $this->read = $read;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUsersJobOffer(): ?UsersJobOffer
    {
        return $this->usersJobOffer;
    }

    public function setUsersJobOffer(?UsersJobOffer $usersJobOffer): self
    {
        $this->usersJobOffer = $usersJobOffer;

        return $this;
    }

    public function getUsersJobOffersStatus(): ?UsersJobOffersStatus
    {
        return $this->usersJobOffersStatus;
    }

    public function setUsersJobOffersStatus(?UsersJobOffersStatus $usersJobOffersStatus): self
    {
        $this->usersJobOffersStatus = $usersJobOffersStatus;

        return $this;
    }
}
