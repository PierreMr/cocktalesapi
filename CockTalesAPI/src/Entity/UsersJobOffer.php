<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersJobOfferRepository")
 */
class UsersJobOffer
{
    /**
     * @Groups("user")
     * @Groups("userJobOffer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups("user")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="usersJobOffers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Groups("user")
     * @Groups("userJobOffer")
     * @ORM\ManyToOne(targetEntity="App\Entity\JobOffer", inversedBy="usersJobOffers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $jobOffer;

    /**
     * @Groups("user")
     * @Groups("userJobOffer")
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Groups("userJobOffer")
     * @ORM\OneToMany(targetEntity="App\Entity\UsersJobOffersStatus", mappedBy="usersJobOffer", orphanRemoval=true)
     */
    private $usersJobOffersStatuses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="usersJobOffer")
     */
    private $notifications;

    public function __construct()
    {
        $this->usersJobOffersStatuses = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getJobOffer(): ?JobOffer
    {
        return $this->jobOffer;
    }

    public function setJobOffer(?JobOffer $jobOffer): self
    {
        $this->jobOffer = $jobOffer;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|UsersJobOffersStatus[]
     */
    public function getUsersJobOffersStatuses(): Collection
    {
        return $this->usersJobOffersStatuses;
    }

    public function addUsersJobOffersStatus(UsersJobOffersStatus $usersJobOffersStatus): self
    {
        if (!$this->usersJobOffersStatuses->contains($usersJobOffersStatus)) {
            $this->usersJobOffersStatuses[] = $usersJobOffersStatus;
            $usersJobOffersStatus->setUsersJobOffer($this);
        }

        return $this;
    }

    public function removeUsersJobOffersStatus(UsersJobOffersStatus $usersJobOffersStatus): self
    {
        if ($this->usersJobOffersStatuses->contains($usersJobOffersStatus)) {
            $this->usersJobOffersStatuses->removeElement($usersJobOffersStatus);
            // set the owning side to null (unless already changed)
            if ($usersJobOffersStatus->getUsersJobOffer() === $this) {
                $usersJobOffersStatus->setUsersJobOffer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setUsersJobOffer($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getUsersJobOffer() === $this) {
                $notification->setUsersJobOffer(null);
            }
        }

        return $this;
    }
}
