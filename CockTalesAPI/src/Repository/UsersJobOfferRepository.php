<?php

namespace App\Repository;

use App\Entity\UsersJobOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsersJobOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersJobOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersJobOffer[]    findAll()
 * @method UsersJobOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersJobOfferRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsersJobOffer::class);
    }

    // /**
    //  * @return UsersJobOffer[] Returns an array of UsersJobOffer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersJobOffer
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
