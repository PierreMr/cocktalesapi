<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\Resume;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ResumeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
         
        for ($i = 0; $i < 10; $i++) {
            $newResume = new Resume();
            $newResume->setText($faker->sentences($nb = 3, $asText = true));
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newResume->setCreatedAt($datetime);
            $newResume->setUpdatedAt($datetime);
            
            $newResume->setUser($this->getReference(User::class.'_'.$i));

            $manager->persist($newResume);
            $this->addReference(Resume::class.'_'.$i, $newResume);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}