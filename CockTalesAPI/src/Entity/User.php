<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

use Faker;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @Groups("user")
     * @Groups("login")
     * @Groups("company")
     * @Groups("companyRate")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Groups("company")
     * @Groups("companyRate")
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min = 2, max = 50)
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Groups("company")
     * @Groups("companyRate")
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min = 2, max = 50)
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Groups("company")
     * @Groups("companyRate")
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Email()
     * @Assert\Length(min = 2, max = 50)
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min = 8, max = 50)
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("string")
     * @Assert\Length(min = 2, max = 50)
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("string")
     * @Assert\Length(min = 2, max = 50)
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("string")
     * @Assert\Length(min = 2, max = 50)
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @Groups("login")
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $apiToken;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Groups("user")
     * @Groups("login")
     * @ORM\Column(type="boolean")
     */
    private $isEmployer;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("datetime")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("datetime")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("array")
     * @ORM\Column(type="array")
     */
    private $roles = [];
    /**
     * @Groups("user")
     * @Groups("login")
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @Groups("user")
     * @ORM\OneToMany(targetEntity="App\Entity\Resume", mappedBy="user", orphanRemoval=true)
     */
    private $resumes;

    /**
     * @Groups("user")
     * @ORM\ManyToMany(targetEntity="App\Entity\Company", mappedBy="user")
     */
    private $companies;

    /**
     * @Groups("user")
     * @ORM\OneToMany(targetEntity="App\Entity\UserCompaniesLike", mappedBy="user", orphanRemoval=true)
     */
    private $userCompaniesLikes;

    /**
     * @Groups("user")
     * @ORM\OneToMany(targetEntity="App\Entity\UsersJobOffer", mappedBy="user", orphanRemoval=true)
     */
    private $usersJobOffers;

    /**
     * @Groups("user")
     * @ORM\OneToMany(targetEntity="App\Entity\UsersJobOffersStatus", mappedBy="user", orphanRemoval=true)
     */
    private $usersJobOffersStatuses;

    /**
     * @Groups("user")
     * @ORM\OneToMany(targetEntity="App\Entity\UserRate", mappedBy="user", orphanRemoval=true)
     */
    private $userRates;

    /**
     * @Groups("user")
     * @ORM\OneToMany(targetEntity="App\Entity\CompanyRate", mappedBy="user", orphanRemoval=true)
     */
    private $companyRates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="user", orphanRemoval=true)
     */
    private $notifications;

    public function __construct()
    {
        $faker = Faker\Factory::create('fr_FR');
        $this->apiToken = $faker->unique()->regexify('[A-Za-z0-9_%]{13}\.[A-Za-z0-9_%]{9}');

        $this->roles = ['ROLE_USER'];

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);
        $this->createdAt = $datetime;
        $this->updatedAt = $datetime;

        $this->resumes = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->userCompaniesLikes = new ArrayCollection();
        $this->usersJobOffers = new ArrayCollection();
        $this->usersJobOffersStatuses = new ArrayCollection();
        $this->userRates = new ArrayCollection();
        $this->companyRates = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    public function setApiToken(?string $apiToken): self
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getIsEmployer(): ?bool
    {
        return $this->isEmployer;
    }

    public function setIsEmployer(?bool $isEmployer): self
    {
        $this->isEmployer = $isEmployer;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
    
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    public function getSalt() {
        
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function eraseCredentials() {
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Resume[]
     */
    public function getResumes(): Collection
    {
        return $this->resumes;
    }

    public function addResume(Resume $resume): self
    {
        if (!$this->resumes->contains($resume)) {
            $this->resumes[] = $resume;
            $resume->setUser($this);
        }

        return $this;
    }

    public function removeResume(Resume $resume): self
    {
        if ($this->resumes->contains($resume)) {
            $this->resumes->removeElement($resume);
            // set the owning side to null (unless already changed)
            if ($resume->getUser() === $this) {
                $resume->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->addUser($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->companies->contains($company)) {
            $this->companies->removeElement($company);
            $company->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserCompaniesLike[]
     */
    public function getUserCompaniesLikes(): Collection
    {
        return $this->userCompaniesLikes;
    }

    public function addUserCompaniesLike(UserCompaniesLike $userCompaniesLike): self
    {
        if (!$this->userCompaniesLikes->contains($userCompaniesLike)) {
            $this->userCompaniesLikes[] = $userCompaniesLike;
            $userCompaniesLike->setUser($this);
        }

        return $this;
    }

    public function removeUserCompaniesLike(UserCompaniesLike $userCompaniesLike): self
    {
        if ($this->userCompaniesLikes->contains($userCompaniesLike)) {
            $this->userCompaniesLikes->removeElement($userCompaniesLike);
            // set the owning side to null (unless already changed)
            if ($userCompaniesLike->getUser() === $this) {
                $userCompaniesLike->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UsersJobOffer[]
     */
    public function getUsersJobOffers(): Collection
    {
        return $this->usersJobOffers;
    }

    public function addUsersJobOffer(UsersJobOffer $usersJobOffer): self
    {
        if (!$this->usersJobOffers->contains($usersJobOffer)) {
            $this->usersJobOffers[] = $usersJobOffer;
            $usersJobOffer->setUser($this);
        }

        return $this;
    }

    public function removeUsersJobOffer(UsersJobOffer $usersJobOffer): self
    {
        if ($this->usersJobOffers->contains($usersJobOffer)) {
            $this->usersJobOffers->removeElement($usersJobOffer);
            // set the owning side to null (unless already changed)
            if ($usersJobOffer->getUser() === $this) {
                $usersJobOffer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UsersJobOffersStatus[]
     */
    public function getUsersJobOffersStatuses(): Collection
    {
        return $this->usersJobOffersStatuses;
    }

    public function addUsersJobOffersStatus(UsersJobOffersStatus $usersJobOffersStatus): self
    {
        if (!$this->usersJobOffersStatuses->contains($usersJobOffersStatus)) {
            $this->usersJobOffersStatuses[] = $usersJobOffersStatus;
            $usersJobOffersStatus->setUser($this);
        }

        return $this;
    }

    public function removeUsersJobOffersStatus(UsersJobOffersStatus $usersJobOffersStatus): self
    {
        if ($this->usersJobOffersStatuses->contains($usersJobOffersStatus)) {
            $this->usersJobOffersStatuses->removeElement($usersJobOffersStatus);
            // set the owning side to null (unless already changed)
            if ($usersJobOffersStatus->getUser() === $this) {
                $usersJobOffersStatus->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRate[]
     */
    public function getUserRates(): Collection
    {
        return $this->userRates;
    }

    public function addUserRate(UserRate $userRate): self
    {
        if (!$this->userRates->contains($userRate)) {
            $this->userRates[] = $userRate;
            $userRate->setUser($this);
        }

        return $this;
    }

    public function removeUserRate(UserRate $userRate): self
    {
        if ($this->userRates->contains($userRate)) {
            $this->userRates->removeElement($userRate);
            // set the owning side to null (unless already changed)
            if ($userRate->getUser() === $this) {
                $userRate->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRate[]
     */
    public function getCompanyRates(): Collection
    {
        return $this->companyRates;
    }

    public function addCompanyRate(CompanyRate $companyRate): self
    {
        if (!$this->companyRates->contains($companyRate)) {
            $this->companyRates[] = $companyRate;
            $companyRate->setUser($this);
        }

        return $this;
    }

    public function removeCompanyRate(CompanyRate $companyRate): self
    {
        if ($this->companyRates->contains($companyRate)) {
            $this->companyRates->removeElement($companyRate);
            // set the owning side to null (unless already changed)
            if ($companyRate->getUser() === $this) {
                $companyRate->setUser(null);
            }
        }

        return $this;
    }

    public function updateUser($user, $body)
    {
        foreach ($body as $key => $value) {
            if ($key == 'firstname') $user->setFirstname($value);
            if ($key == 'lastname') $user->setLastname($value);
            if ($key == 'email') $user->setEmail($value);
            if ($key == 'phone') $user->setPhone($value);
            if ($key == 'address') $user->setAddress($value);
            if ($key == 'zip') $user->setZip($value);
            if ($key == 'city') $user->setCity($value);
            if ($key == 'image') $user->setImage($value);
            // if ($key == 'token') $user->setToken($value);
            // if ($key == 'roles') $user->setRoles($value);
        }

        return $user;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setUser($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getUser() === $this) {
                $notification->setUser(null);
            }
        }

        return $this;
    }
}
