<?php

namespace App\Models;

class UsersJobOfferModel
{
    private $id;

    private $user;

    private $jobOffer;

    private $comment;

    private $createdAt;

    private $updatedAt;

    public function __construct()
    {
        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);
        $this->status = true;
        $this->createdAt = $datetime;
        $this->updatedAt = $datetime;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?int
    {
        return $this->user;
    }

    public function setUser(?int $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getJobOffer(): ?int
    {
        return $this->jobOffer;
    }

    public function setJobOffer(?int $jobOffer): self
    {
        $this->jobOffer = $jobOffer;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
