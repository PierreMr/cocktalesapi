<?php

namespace App\Controller;

use App\Entity\UsersJobOffer;
use App\Entity\User;
use App\Repository\UsersJobOfferRepository;
use App\Models\UsersJobOfferModel;
use App\Repository\UserRepository;
use App\Repository\JobOfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/usersjoboffer")
 */
class UsersJobOfferController extends AbstractController
{
    private $usersJobOfferRepository;
    private $serializer;
    private $usersJobOfferModel;
    private $userRepository;
    private $jobOfferRepository;
        
	public function __construct(
        UsersJobOfferRepository $usersJobOfferRepository,
        SerializerInterface $serializer,
        UsersJobOfferModel $usersJobOfferModel,
        UserRepository $userRepository,
        JobOfferRepository $jobOfferRepository
    )
    {
		$this->usersJobOfferRepository = $usersJobOfferRepository;
		$this->serializer = $serializer;
		$this->usersJobOfferModel = $usersJobOfferModel;
		$this->userRepository = $userRepository;
		$this->jobOfferRepository = $jobOfferRepository;
    }
    
    /**
     * @Route("/", name="user_job_offer_index", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $usersJobOffers = $this->usersJobOfferRepository->findAll();

        $serialize = $this->serializer->serialize($usersJobOffers, 'json', ['groups' => 'userJobOffer']);
        $results = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($results);
    }

    /**
     * @Route("/new", name="user_job_offer_new", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {
        $body = $request->getContent();

        $usersJobOfferModel = $this->serializer->deserialize($body, UsersJobOfferModel::class, 'json');
        $user = $this->userRepository->find($usersJobOfferModel->getUser());
        $jobOffer = $this->jobOfferRepository->find($usersJobOfferModel->getJobOffer());

        $usersJobOffer = new UsersJobOffer();

        $usersJobOffer->setComment($usersJobOfferModel->getComment());
        $usersJobOffer->setCreatedAt($usersJobOfferModel->getCreatedAt());
        $usersJobOffer->setUpdatedAt($usersJobOfferModel->getUpdatedAt());
        $usersJobOffer->setUser($user);
        $usersJobOffer->setJobOffer($jobOffer);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($usersJobOffer);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($usersJobOffer, 'json', ['groups' => 'userJobOffer']));
    }

    /**
     * @Route("/{id}", name="user_job_offer_show", methods={"GET"})
     */
    public function show(JobOffer $usersJobOffer = null): JsonResponse
    {   
        if (!$usersJobOffer) {
            return new JsonResponse(["message" => "Invalid UsersjobOffer number"]);
        }

        $serialize = $this->serializer->serialize($usersJobOffer, 'json', ['groups' => 'userJobOffer']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/edit", name="user_job_offer_edit", methods={"PUT"})
     */
    public function edit(Request $request, UsersJobOffer $usersJobOffer): JsonResponse
    {
        $body = $this->serializer->decode($request->getContent(), 'json');
        
        $usersJobOffer->updateJobOffer($usersJobOffer, $body);

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);

        $usersJobOffer->setUpdatedAt($datetime);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($usersJobOffer);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($usersJobOffer, 'json', ['groups' => 'userJobOffer']));  
    }

    /**
     * @Route("/{id}", name="user_job_offer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UsersJobOffer $usersJobOffer): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($usersJobOffer);
        $entityManager->flush();

        return new JsonResponse();
    }
}
