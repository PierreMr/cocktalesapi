<?php

namespace App\Listener;

use App\Exception\ResourceNotFoundException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ResourceNotFoundExceptionListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException'
        ];
    }
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();
        if (!$exception instanceof ResourceNotFoundException) {
            return;
        }
        $error = [
            'error' => [
                'reason' => $exception->getMessage(),
                'code' => 404,
            ],
        ];
        $event->setResponse(new JsonResponse($error, 404));
    }
}