<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SecurityController extends AbstractController
{

	private $userRepository;
    private $serializer;
    private $validator;
    private $passwordEncoder;
    
	public function __construct(
        UserRepository $userRepository,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
		$this->userRepository = $userRepository;
		$this->serializer = $serializer;
		$this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(Request $request): JsonResponse
    {
        $body = $request->getContent();

        $body = $this->serializer->decode($body, 'json');

        $email = $body['email'];
        $password = $body['password'];

        $user = $this->userRepository->findOneBy(['email' => $email]);

        if ($user) {
            if ($this->passwordEncoder->isPasswordValid($user, $password)) {
                return new JsonResponse($this->serializer->normalize($user, 'json', ['groups' => 'login']));
            }
            else {
                return new JsonResponse(["error" => true, "message" => "Mauvais mot de passe."]);
            }
        }
        else {
            return new JsonResponse(["error" => true, "message" => "Adresse email inconnue."]);
        }
    }
}