<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UsersJobOffer;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Exception\ValidationException;
use Faker;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

	private $userRepository;
    private $serializer;
    private $validator;
    private $passwordEncoder;
    
	public function __construct(
        UserRepository $userRepository,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
		$this->userRepository = $userRepository;
		$this->serializer = $serializer;
		$this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
    }
    
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $users = $this->userRepository->findAll();

        $serialize = $this->serializer->serialize($users, 'json', ['groups' => 'user']);
        $results = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($results);
    }

    /**
     * @Route("/new", name="user_new", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {
        $body = $request->getContent();
        
        $user = $this->serializer->deserialize($body, User::class, 'json');

        if (\count($violations = $this->validator->validate($user))) {
            throw new ValidationException($violations);
        }

        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $user->getPassword()
            )
        );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($user, 'json', ['groups' => 'user']));
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user = null): JsonResponse
    {   
        if (!$user) {
            return new JsonResponse(["message" => "User not found"]);
        }

        $serialize = $this->serializer->serialize($user, 'json', ['groups' => 'user']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"PUT"})
     */
    public function edit(Request $request, User $user): JsonResponse
    {
        $body = $this->serializer->decode($request->getContent(), 'json');
        
        $user->updateUser($user, $body);

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);

        $user->setUpdatedAt($datetime);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($user, 'json', ['groups' => 'user']));  
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/joboffers/{id}", name="user_job_offers", methods={"GET"})
     */
    public function getUsersJobOffer(User $user): JsonResponse
    {
        $usersJobOffer = $user->getUsersJobOffers();

        return new JsonResponse($this->serializer->normalize($usersJobOffer, 'json', ['groups' => 'userJobOffer']));
    }
}
