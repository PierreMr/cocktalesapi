<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\Company;
use App\Entity\UserCompaniesLike;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class UserCompaniesLikeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
         
        for ($i = 0; $i < 20; $i++) {
            $newUserCompaniesLike = new UserCompaniesLike();
            
            $newUserCompaniesLike->setUser($this->getReference(User::class.'_'.rand(0, 9)));
            $newUserCompaniesLike->setCompany($this->getReference(Company::class.'_'.rand(0, 19)));
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newUserCompaniesLike->setCreatedAt($datetime);
            $newUserCompaniesLike->setUpdatedAt($datetime);
            
            $manager->persist($newUserCompaniesLike);
            $this->addReference(UserCompaniesLike::class.'_'.$i, $newUserCompaniesLike);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, CompanyFixtures::class];
    }
}