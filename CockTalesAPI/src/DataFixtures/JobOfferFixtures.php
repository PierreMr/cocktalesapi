<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\Company;
use App\Entity\JobOffer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class JobOfferFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
         
        for ($i = 0; $i < 20; $i++) {
            $newJobOffer = new JobOffer();

            $newJobOffer->setTitle($faker->jobTitle());

            $date = $faker->dateTimeThisDecade($max = 'now', $timezone = 'Europe/Paris');
            $startsAt = new \DateTimeImmutable($date->format('Y-m-d H:i:s'));
            
            $hours = rand(1, 6);
            $minutes = $faker->randomElement($array = [0, 15, 30, 45]);
            $endsAt = $date->add(new \DateInterval('PT'.$hours.'H'.$minutes.'M'));

            $schedule = [['startsAt' => $startsAt, 'endsAt' => $endsAt]];

            for ($j = 1; $j < rand(1, 3); $j++) {
                $startsAt = $date->add(new \DateInterval('P'.$j.'D'));
                $endsAt = $date->add(new \DateInterval('P'.$j.'DT'.$hours.'H'.$minutes.'M'));

                array_push($schedule, ['startsAt' => $startsAt, 'endsAt' => $endsAt]);
            }

            $newJobOffer->setSchedule($schedule);

            $newJobOffer->setHourlyWadge($faker->randomFloat($nbMaxDecimals = 1, $min = 8, $max = 15));
            $newJobOffer->setDescription($faker->optional()->sentences($nb = 3, $asText = true));
            $newJobOffer->setStatus($faker->boolean($chanceOfGettingTrue = 75));
            $newJobOffer->setCity($faker->city());
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newJobOffer->setCreatedAt($datetime);
            $newJobOffer->setUpdatedAt($datetime);
            
            $newJobOffer->setCompany($this->getReference(Company::class.'_'.rand(0, 19)));

            $manager->persist($newJobOffer);
            $this->addReference(JobOffer::class.'_'.$i, $newJobOffer);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, CompanyFixtures::class];
    }
}