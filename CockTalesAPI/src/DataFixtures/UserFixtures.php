<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR'); 
        $users = [
            ['email' => 'mauerpierre@gmail.com', 'password' => '123456', 'lastname' => 'Mauer', 'firstname' => 'Pierre', 'apiToken' => 'apiTokenPierre', 'isEmployer' => true],
            ['email' => 'julien.laville@hotmail.fr', 'password' => '123456', 'lastname' => 'Laville', 'firstname' => 'Julien', 'apiToken' => 'apiTokenJulien', 'isEmployer' => false],
        ];

        foreach ($users as $key => $user) {
            $newUser = new User();
            $newUser->setEmail($user['email']);
            $newUser->setPassword(
                $this->passwordEncoder->encodePassword(
                    $newUser,
                    $user['password']
                )
            );
            $newUser->setLastname($user['lastname']);
            $newUser->setFirstname($user['firstname']);
            $address = $faker->optional()->streetAddress;
            $newUser->setAddress($address);
            if ($address) {
                $newUser->setZip($faker->postcode);
                $newUser->setCity($faker->city);
            }
            $newUser->setPhone($faker->optional()->phoneNumber);
            $newUser->setApiToken($user['apiToken']);
            $newUser->setImage($faker->imageUrl());
            $newUser->setIsEmployer($user['isEmployer']);
            // $newUser->setImage($faker->image($dir = 'public/images/user/', $width = 640, $height = 480, '', false));
            $newUser->setUsername($user['firstname'].'.'.$user['lastname']);
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newUser->setCreatedAt($datetime);
            $newUser->setUpdatedAt($datetime);
            
            $manager->persist($newUser);

            $this->addReference(User::class.'_'.$key, $newUser);
        }
 
        for ($i = count($users); $i < (count($users) + 8); $i++) {
            $newUser = new User();
            $firstName = $faker->firstName;
            $lastName = $faker->lastName;
            $newUser->setEmail($faker->unique()->email);
            $newUser->setPassword(
                $this->passwordEncoder->encodePassword(
                    $newUser,
                    '123456'
                )
            );
            $newUser->setFirstname($firstName);
            $newUser->setLastname($lastName);
            $address = $faker->optional()->streetAddress;
            $newUser->setAddress($address);
            if ($address) {
                $newUser->setZip($faker->postcode);
                $newUser->setCity($faker->city);
            }
            $newUser->setPhone($faker->optional()->phoneNumber);
            $newUser->setApiToken($faker->unique()->regexify('[A-Za-z0-9_%]{13}\.[A-Za-z0-9_%]{9}'));
            $newUser->setImage($faker->imageUrl());
            $newUser->setIsEmployer($faker->boolean($chanceOfGettingTrue = 15));
            $newUser->setUsername($firstName.'.'.$lastName);
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newUser->setCreatedAt($datetime);
            $newUser->setUpdatedAt($datetime);
            
            $manager->persist($newUser);
            $this->addReference(User::class.'_'.$i, $newUser);
        }
        
        $manager->flush();
    }
}