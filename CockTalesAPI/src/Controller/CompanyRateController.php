<?php

namespace App\Controller;

use App\Entity\CompanyRate;
use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRateRepository;
use App\Repository\UserRepository;
use App\Repository\CompanyRepository;
use App\Models\CompanyRateModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/companyrate")
 */
class CompanyRateController extends AbstractController
{

	private $companyRateRepository;
    private $serializer;
    private $companyRateModel;
	private $companyRepository;
	private $userRepository;
        
	public function __construct(
        CompanyRateRepository $companyRateRepository,
        SerializerInterface $serializer,
        CompanyRateModel $companyRateModel,
        CompanyRepository $companyRepository,
        UserRepository $userRepository
    )
    {
		$this->companyRateRepository = $companyRateRepository;
		$this->serializer = $serializer;
		$this->companyRateModel = $companyRateModel;
		$this->companyRepository = $companyRepository;
		$this->userRepository = $userRepository;
    }
    
    /**
     * @Route("/", name="company_rate_index", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $companyRates = $this->companyRateRepository->findAll();

        $serialize = $this->serializer->serialize($companyRates, 'json', ['groups' => 'companyRate']);
        $results = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($results);
    }

    /**
     * @Route("/new", name="company_rate_new", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {
        $body = $request->getContent();

        $companyRateModel = $this->serializer->deserialize($body, CompanyRateModel::class, 'json');
        $user = $this->userRepository->find($companyRateModel->getUser());
        $company = $this->companyRepository->find($companyRateModel->getCompany());

        $companyRate = new CompanyRate();

        $companyRate->setRate($companyRateModel->getRate());
        $companyRate->setComment($companyRateModel->getComment());
        $companyRate->setCreatedAt($companyRateModel->getCreatedAt());
        $companyRate->setUpdatedAt($companyRateModel->getUpdatedAt());
        $companyRate->setUser($user);
        $companyRate->setCompany($company);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($companyRate);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($companyRate, 'json', ['groups' => 'companyRate']));
    }

    /**
     * @Route("/{id}", name="company_rate_show", methods={"GET"})
     */
    public function show(CompanyRate $companyRate = null): JsonResponse
    {   
        if (!$companyRate) {
            return new JsonResponse(["message" => "Invalid company rate number"]);
        }

        $serialize = $this->serializer->serialize($companyRate, 'json', ['groups' => 'companyRate']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/edit", name="company_rate_edit", methods={"PUT"})
     */
    public function edit(Request $request, CompanyRate $companyRate): JsonResponse
    {
        $body = $this->serializer->decode($request->getContent(), 'json');
        
        $companyRate->updateCompany($companyRate, $body);

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);

        $companyRate->setUpdatedAt($datetime);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($companyRate);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($companyRate, 'json', ['groups' => 'companyRate']));
    }

    /**
     * @Route("/{id}", name="company_rate_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CompanyRate $companyRate): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($companyRate);
        $entityManager->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/company/{id}", name="company_rate_company", methods={"GET"})
     */
    public function company(Company $company = null): JsonResponse
    {   
        if (!$company) {
            return new JsonResponse(["message" => "Invalid company number"]);
        }

        $companyRate = $this->companyRateRepository->findBy(['company' => $company->getId()]);

        $serialize = $this->serializer->serialize($companyRate, 'json', ['groups' => 'companyRate']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }
}
