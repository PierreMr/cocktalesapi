<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("jobOffer")
     * @Groups("companyRate")
     * @Groups("userJobOffer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("jobOffer")
     * @Groups("companyRate")
     * @Groups("userJobOffer")
     * @ORM\ManyToOne(targetEntity="App\Entity\CompanyType", inversedBy="companies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $companyType;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("companyRate")
     * @Groups("jobOffer")
     * @Groups("userJobOffer")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("jobOffer")
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("jobOffer")
     * @ORM\Column(type="string", length=255)
     */
    private $zip;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("jobOffer")
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("jobOffer")
     * @ORM\Column(type="float")
     */
    private $lat;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("companyType")
     * @Groups("jobOffer")
     * @ORM\Column(type="float")
     */
    private $lng;

    /**
     * @Groups("company")
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $theme;

    /**
     * @Groups("company")
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @Groups("company")
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @Groups("company")
     * @Groups("user")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @Groups("company")
     * @Groups("user")
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Groups("company")
     * @Groups("user")
     * @Groups("jobOffer")
     * @Groups("userJobOffer")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Groups("company")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Groups("company")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Groups("company")
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="companies")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\JobOffer", mappedBy="company", orphanRemoval=true)
     */
    private $jobOffers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCompaniesLike", mappedBy="company", orphanRemoval=true)
     */
    private $userCompaniesLikes;

    /**
     * @Groups("company")
     * @ORM\OneToMany(targetEntity="App\Entity\CompanyRate", mappedBy="company", orphanRemoval=true)
     */
    private $companyRates;

    public function __construct()
    {
        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);
        $this->createdAt = $datetime;
        $this->updatedAt = $datetime;

        $this->user = new ArrayCollection();
        $this->jobOffers = new ArrayCollection();
        $this->userCompaniesLikes = new ArrayCollection();
        $this->companyRates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyType(): ?CompanyType
    {
        return $this->companyType;
    }

    public function setCompanyType(?CompanyType $companyType): self
    {
        $this->companyType = $companyType;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    /**
     * @return Collection|JobOffer[]
     */
    public function getJobOffers(): Collection
    {
        return $this->jobOffers;
    }

    public function addJobOffer(JobOffer $jobOffer): self
    {
        if (!$this->jobOffers->contains($jobOffer)) {
            $this->jobOffers[] = $jobOffer;
            $jobOffer->setCompany($this);
        }

        return $this;
    }

    public function removeJobOffer(JobOffer $jobOffer): self
    {
        if ($this->jobOffers->contains($jobOffer)) {
            $this->jobOffers->removeElement($jobOffer);
            // set the owning side to null (unless already changed)
            if ($jobOffer->getCompany() === $this) {
                $jobOffer->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCompaniesLike[]
     */
    public function getUserCompaniesLikes(): Collection
    {
        return $this->userCompaniesLikes;
    }

    public function addUserCompaniesLike(UserCompaniesLike $userCompaniesLike): self
    {
        if (!$this->userCompaniesLikes->contains($userCompaniesLike)) {
            $this->userCompaniesLikes[] = $userCompaniesLike;
            $userCompaniesLike->setCompany($this);
        }

        return $this;
    }

    public function removeUserCompaniesLike(UserCompaniesLike $userCompaniesLike): self
    {
        if ($this->userCompaniesLikes->contains($userCompaniesLike)) {
            $this->userCompaniesLikes->removeElement($userCompaniesLike);
            // set the owning side to null (unless already changed)
            if ($userCompaniesLike->getCompany() === $this) {
                $userCompaniesLike->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRate[]
     */
    public function getCompanyRates(): Collection
    {
        return $this->companyRates;
    }

    public function addCompanyRate(CompanyRate $companyRate): self
    {
        if (!$this->companyRates->contains($companyRate)) {
            $this->companyRates[] = $companyRate;
            $companyRate->setCompany($this);
        }

        return $this;
    }

    public function removeCompanyRate(CompanyRate $companyRate): self
    {
        if ($this->companyRates->contains($companyRate)) {
            $this->companyRates->removeElement($companyRate);
            // set the owning side to null (unless already changed)
            if ($companyRate->getCompany() === $this) {
                $companyRate->setCompany(null);
            }
        }

        return $this;
    }

    public function updateCompany($company, $body)
    {
        foreach ($body as $key => $value) {
            if ($key == 'name') $company->setName($value);
            if ($key == 'address') $company->setAddress($value);
            if ($key == 'zip') $company->setZip($value);
            if ($key == 'city') $company->setCity($value);
            if ($key == 'lat') $company->setLat($value);
            if ($key == 'lng') $company->setLng($value);
            if ($key == 'theme') $company->setTheme($value);
            if ($key == 'phone') $company->setPhone($value);
            if ($key == 'email') $company->setEmail($value);
            if ($key == 'website') $company->setWebsite($value);
            if ($key == 'description') $company->setDescription($value);
            if ($key == 'image') $company->setImage($value);
        }

        return $company;
    }
}
