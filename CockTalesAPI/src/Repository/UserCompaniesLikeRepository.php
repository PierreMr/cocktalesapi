<?php

namespace App\Repository;

use App\Entity\UserCompaniesLike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserCompaniesLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCompaniesLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCompaniesLike[]    findAll()
 * @method UserCompaniesLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCompaniesLikeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserCompaniesLike::class);
    }

    // /**
    //  * @return UserCompaniesLike[] Returns an array of UserCompaniesLike objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCompaniesLike
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
