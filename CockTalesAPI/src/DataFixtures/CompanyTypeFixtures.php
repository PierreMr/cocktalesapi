<?php
namespace App\DataFixtures;
 
use App\Entity\CompanyType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CompanyTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
        
        $types = [
            ['label' => 'Bar'],
            ['label' => 'Restaurant'],
        ];

        foreach ($types as $key => $type) {
            $newCompanyType = new CompanyType();
            $newCompanyType->setLabel($type['label']);
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newCompanyType->setCreatedAt($datetime);
            $newCompanyType->setUpdatedAt($datetime);
            
            $manager->persist($newCompanyType);

            $this->addReference(CompanyType::class.'_'.$key, $newCompanyType);
        }

        $manager->flush();
    }
}