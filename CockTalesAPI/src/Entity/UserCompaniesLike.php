<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserCompaniesLikeRepository")
 */
class UserCompaniesLike
{
    /**
     * @Groups("user")
     * @Groups("company")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups("user")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("company")
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userCompaniesLikes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Groups("user")
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="userCompaniesLikes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
