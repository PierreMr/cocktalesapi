<?php

namespace App\Controller;

use App\Entity\CompanyType;
use App\Repository\CompanyTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/companytype")
 */
class CompanyTypeController extends AbstractController
{

	private $companyTypeRepository;
    private $serializer;
    
	public function __construct(
        CompanyTypeRepository $companyTypeRepository,
        SerializerInterface $serializer
    )
    {
		$this->companyTypeRepository = $companyTypeRepository;
		$this->serializer = $serializer;
    }
    
    /**
     * @Route("/", name="companyType_index", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $companyTypes = $this->companyTypeRepository->findAll();

        $serialize = $this->serializer->serialize($companyTypes, 'json', ['groups' => 'companyType']);
        $results = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($results);
    }

    /**
     * @Route("/new", name="companyType_new", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {
        $body = $request->getContent();
        
        $companyType = $this->serializer->deserialize($body, CompanyType::class, 'json');

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($companyType);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($companyType));
    }

    /**
     * @Route("/{id}", name="companyType_show", methods={"GET"})
     */
    public function show(CompanyType $companyType = null): JsonResponse
    {   
        if (!$companyType) {
            return new JsonResponse(["message" => "Invalid Company type number"]);
        }

        $serialize = $this->serializer->serialize($companyType, 'json', ['groups' => 'companyType']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/edit", name="companyType_edit", methods={"PUT"})
     */
    public function edit(Request $request, CompanyType $companyType): JsonResponse
    {
        $body = $this->serializer->decode($request->getContent(), 'json');
        
        $companyType->updateCompanyType($companyType, $body);

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);

        $companyType->setUpdatedAt($datetime);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($companyType);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($companyType, 'json', ['groups' => 'companyType']));  
    }

    /**
     * @Route("/{id}", name="companyType_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CompanyType $companyType): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($companyType);
        $entityManager->flush();

        return new JsonResponse();
    }
}
