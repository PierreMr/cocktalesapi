<?php

namespace App\Models;

class CompanyRateModel
{
    private $rate;

    private $comment;

    private $createdAt;

    private $updatedAt;

    private $company;

    private $user;
    

    public function __construct()
    {
        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);
        $this->createdAt = $datetime;
        $this->updatedAt = $datetime;
    }public function getId(): ?int
    {
        return $this->id;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getUser(): ?int
    {
        return $this->User;
    }

    public function setUser(?int $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getCompany(): ?int
    {
        return $this->company;
    }

    public function setCompany(?int $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
