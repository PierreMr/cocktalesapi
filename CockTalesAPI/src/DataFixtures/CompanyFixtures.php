<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\CompanyType;
use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CompanyFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');

        $cities = [
            [
                'latMin' => 48.83370272345498,
                'latMax' => 48.88475067793349,
                'lonMin' => 2.2900743330078512,
                'lonMax' => 2.3855180585937887,
            ],
        ];
         
        for ($i = 0; $i < 20; $i++) {
            $city = rand(0, count($cities) - 1);

            $newCompany = new Company();
            $newCompany->setName($faker->company);
            $newCompany->setAddress($faker->streetAddress);
            $newCompany->setZip($faker->postcode);
            $newCompany->setCity($faker->city);
            $newCompany->setLat($faker->latitude($min = $cities[$city]['latMin'], $max = $cities[$city]['latMax']));
            $newCompany->setLng($faker->longitude($min = $cities[$city]['lonMin'], $max = $cities[$city]['lonMax']));
            $newCompany->setDescription($faker->optional()->sentences($nb = 3, $asText = true));
            $newCompany->setPhone($faker->optional()->phoneNumber);
            $newCompany->setEmail($faker->optional()->email);
            $newCompany->setWebsite($faker->optional()->freeEmailDomain);
            $newCompany->setImage($faker->imageUrl());
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newCompany->setCreatedAt($datetime);
            $newCompany->setUpdatedAt($datetime);
            
            $newCompany->setCompanyType($this->getReference(CompanyType::class.'_'.rand(0, 1)));
            $newCompany->addUser($this->getReference(User::class.'_'.rand(0, 9)));

            $manager->persist($newCompany);
            $this->addReference(Company::class.'_'.$i, $newCompany);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, CompanyTypeFixtures::class];
    }
}