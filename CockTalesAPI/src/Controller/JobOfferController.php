<?php

namespace App\Controller;

use App\Entity\JobOffer;
use App\Entity\User;
use App\Repository\JobOfferRepository;
use App\Models\JobOfferModel;
use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/joboffer")
 */
class JobOfferController extends AbstractController
{
    private $jobOfferRepository;
    private $serializer;
    private $jobOfferModel;
    private $companyRepository;
        
	public function __construct(
        JobOfferRepository $jobOfferRepository,
        SerializerInterface $serializer,
        JobOfferModel $jobOfferModel,
        CompanyRepository $companyRepository
    )
    {
		$this->jobOfferRepository = $jobOfferRepository;
		$this->serializer = $serializer;
		$this->jobOfferModel = $jobOfferModel;
		$this->companyRepository = $companyRepository;
    }
    
    /**
     * @Route("/", name="job_offer_index", methods={"GET"})
     */
    public function index(): JsonResponse
    {
        $jobOffers = $this->jobOfferRepository->findAll();

        $serialize = $this->serializer->serialize($jobOffers, 'json', ['groups' => 'jobOffer']);
        $results = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($results);
    }

    /**
     * @Route("/new", name="job_offer_new", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {
        $body = $request->getContent();

        $jobOfferModel = $this->serializer->deserialize($body, JobOfferModel::class, 'json');
        $company = $this->companyRepository->find($jobOfferModel->getCompany());

        $jobOffer = new JobOffer();
        
        $jobOffer->setTitle($jobOfferModel->getTitle());
        
        $schedule = $jobOfferModel->getSchedule();
        $panels = [];
        foreach ($schedule as $panel) {
            array_push($panels, ["startsAt" => new \Datetime($panel['startsAt']), "endsAt" => new \Datetime($panel['endsAt'])]);
        }
        $jobOffer->setSchedule($panels);

        $jobOffer->setHourlyWadge($jobOfferModel->getHourlyWadge());
        $jobOffer->setDescription($jobOfferModel->getDescription());
        $jobOffer->setStatus($jobOfferModel->getStatus());
        $jobOffer->setCity($jobOfferModel->getCity());
        $jobOffer->setCreatedAt($jobOfferModel->getCreatedAt());
        $jobOffer->setUpdatedAt($jobOfferModel->getUpdatedAt());
        $jobOffer->setCompany($company);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($jobOffer);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($jobOffer, 'json', ['groups' => 'jobOffer']));
    }

    /**
     * @Route("/{id}", name="job_offer_show", methods={"GET"})
     */
    public function show(JobOffer $jobOffer = null): JsonResponse
    {   
        if (!$jobOffer) {
            return new JsonResponse(["message" => "Invalid jobOffer number"]);
        }

        $serialize = $this->serializer->serialize($jobOffer, 'json', ['groups' => 'jobOffer']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/edit", name="job_offer_edit", methods={"PUT"})
     */
    public function edit(Request $request, JobOffer $jobOffer): JsonResponse
    {
        $body = $this->serializer->decode($request->getContent(), 'json');
        
        $jobOffer->updateJobOffer($jobOffer, $body);

        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);

        $jobOffer->setUpdatedAt($datetime);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($jobOffer);
        $entityManager->flush();

        return new JsonResponse($this->serializer->normalize($jobOffer, 'json', ['groups' => 'jobOffer']));  
    }

    /**
     * @Route("/{id}", name="job_offer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, JobOffer $jobOffer): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($jobOffer);
        $entityManager->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/user/{id}", name="job_offer_user_list", methods={"GET"})
     */
    public function user(User $user = null): JsonResponse
    {   
        if (!$user) {
            return new JsonResponse(["message" => "Invalid user number"]);
        }

        $jobOffers = [];

        $companies = $user->getCompanies();

        foreach ($companies as $company) {
            $jobOffersCompany = $company->getJobOffers();
            foreach ($jobOffersCompany as $jobOffer) {
                array_push($jobOffers, $jobOffer);
            }
        }

        $serialize = $this->serializer->serialize($jobOffers, 'json', ['groups' => 'jobOffer']);
        $result = $this->serializer->decode($serialize, 'json');

        return new JsonResponse($result);
    }
}
