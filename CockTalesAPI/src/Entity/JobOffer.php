<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JobOfferRepository")
 */
class JobOffer
{
    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("jobOffer")
     * @Groups("userJobOffer")
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="jobOffers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Column(type="json_array")
     */
    private $schedule;

    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Column(type="float")
     */
    private $hourlyWadge;

    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @Groups("jobOffer")
     * @Groups("user")
     * @Groups("company")
     * @Groups("userJobOffer")
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @Groups("jobOffer")
     * @Groups("userJobOffer")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Groups("jobOffer")
     * @Groups("userJobOffer")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UsersJobOffer", mappedBy="jobOffer", orphanRemoval=true)
     */
    private $usersJobOffers;

    public function __construct()
    {
        $this->status = true;
        $this->usersJobOffers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSchedule()
    {
        return $this->schedule;
    }

    public function setSchedule($schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getHourlyWadge(): ?float
    {
        return $this->hourlyWadge;
    }

    public function setHourlyWadge(float $hourlyWadge): self
    {
        $this->hourlyWadge = $hourlyWadge;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|UsersJobOffer[]
     */
    public function getUsersJobOffers(): Collection
    {
        return $this->usersJobOffers;
    }

    public function addUsersJobOffer(UsersJobOffer $usersJobOffer): self
    {
        if (!$this->usersJobOffers->contains($usersJobOffer)) {
            $this->usersJobOffers[] = $usersJobOffer;
            $usersJobOffer->setJobOffer($this);
        }

        return $this;
    }

    public function removeUsersJobOffer(UsersJobOffer $usersJobOffer): self
    {
        if ($this->usersJobOffers->contains($usersJobOffer)) {
            $this->usersJobOffers->removeElement($usersJobOffer);
            // set the owning side to null (unless already changed)
            if ($usersJobOffer->getJobOffer() === $this) {
                $usersJobOffer->setJobOffer(null);
            }
        }

        return $this;
    }

    public function updateJobOffer($jobOffer, $body)
    {
        foreach ($body as $key => $value) {
            if ($key == 'title') $jobOffer->setTitle($value);
            if ($key == 'schedule') $jobOffer->setSchedule($value);
            if ($key == 'hourlyWadge') $jobOffer->setHourlyWadge($value);
            if ($key == 'description') $jobOffer->setDescription($value);
            if ($key == 'status') $jobOffer->setStatus($value);
            if ($key == 'city') $jobOffer->setCity($value);
        }

        return $jobOffer;
    }
}
