<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyTypeRepository")
 */
class CompanyType
{
    /**
     * @Groups("companyType")
     * @Groups("company")
     * @Groups("user")
     * @Groups("jobOffer")
     * @Groups("companyRate")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("companyType")
     * @Groups("user")
     * @Groups("company")
     * @Groups("jobOffer")
     * @Groups("companyRate")
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @Groups("companyType")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Groups("companyType")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Groups("companyType")
     * @ORM\OneToMany(targetEntity="App\Entity\Company", mappedBy="companyType", orphanRemoval=true)
     */
    private $companies;

    public function __construct()
    {
        $datetime = new \DateTime('now');
        $timezone = new \DateTimeZone('Europe/Paris');
        $datetime->setTimezone($timezone);
        $this->createdAt = $datetime;
        $this->updatedAt = $datetime;

        $this->companies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->setCompanyType($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->companies->contains($company)) {
            $this->companies->removeElement($company);
            // set the owning side to null (unless already changed)
            if ($company->getCompanyType() === $this) {
                $company->setCompanyType(null);
            }
        }

        return $this;
    }

    public function updateCompanyType($companyType, $body)
    {
        foreach ($body as $key => $value) {
            if ($key == 'rate') $companyType->setRate($value);
            if ($key == 'comment') $companyType->setComment($value);
        }

        return $companyType;
    }
}
