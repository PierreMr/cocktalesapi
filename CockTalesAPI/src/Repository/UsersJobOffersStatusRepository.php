<?php

namespace App\Repository;

use App\Entity\UsersJobOffersStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsersJobOffersStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersJobOffersStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersJobOffersStatus[]    findAll()
 * @method UsersJobOffersStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersJobOffersStatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsersJobOffersStatus::class);
    }

    // /**
    //  * @return UsersJobOffersStatus[] Returns an array of UsersJobOffersStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersJobOffersStatus
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
