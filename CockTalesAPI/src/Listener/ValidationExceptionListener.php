<?php

namespace App\Listener;

use App\Exception\ValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Validator\ConstraintViolation;

class ValidationExceptionListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException'
        ];
    }
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();
        if (!$exception instanceof ValidationException) {
            return;
        }
        $violations = [];
        /** @var ConstraintViolation $violation */
        foreach ($exception->getConstraintViolationList() as $violation) {
            $violations[] = [
                'path' => $violation->getPropertyPath(),
                'reason' => $violation->getMessage(),
                'invalidValue' => $violation->getInvalidValue(),
            ];
        }
        $error = [
            'error' => [
                'reason' => 'Invalid request content',
                'code' => 422,
                'violations' => $violations,
            ],
        ];
        $event->setResponse(new JsonResponse($error, 422));
    }
}