# CockTalesAPI


## Setup 

* ```cd CockTalesAPI```
* ```composer install```
* ```cd ../```
* ```docker-compose up -d```

------

* ```docker-compose exec php-fpm bash```
	* ```php bin/console d:s:u --force```
	* ```php bin/console doctrine:fixtures:load -n```


## DataFixtures

* [https://symfonycasts.com/screencast/doctrine-relations/fixture-references](https://symfonycasts.com/screencast/doctrine-relations/fixture-references)
* [https://github.com/fzaninotto/Faker](https://github.com/fzaninotto/Faker)
* ```docker-compose exec php-fpm bash```
	* ```php bin/console doctrine:schema:drop --force && bin/console doctrine:schema:update --force && bin/console doctrine:fixtures:load -n```


## ToDo

### Errors

* Route Not Found
* Request
	* No Authorization
	* Token Not Found
	* Missing Body
	* Missing Informations in body
	* Entity Not Found