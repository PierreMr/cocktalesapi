<?php
namespace App\DataFixtures;
 
use App\Entity\User;
use App\Entity\JobOffer;
use App\Entity\UsersJobOffer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class UsersJobOfferFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) {
        $faker = Faker\Factory::create('fr_FR');
         
        for ($i = 0; $i < 20; $i++) {
            $newUsersJobOffer = new UsersJobOffer();
            
            $newUsersJobOffer->setUser($this->getReference(User::class.'_'.rand(0, 9)));
            $newUsersJobOffer->setJobOffer($this->getReference(JobOffer::class.'_'.rand(0, 19)));

            $newUsersJobOffer->setComment($faker->optional()->sentences($nb = 3, $asText = true));
            
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Paris');
            $datetime->setTimezone($timezone);

            $newUsersJobOffer->setCreatedAt($datetime);
            $newUsersJobOffer->setUpdatedAt($datetime);

            $manager->persist($newUsersJobOffer);
            $this->addReference(UsersJobOffer::class.'_'.$i, $newUsersJobOffer);
        }
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class, JobOfferFixtures::class];
    }
}