<?php

namespace App\Repository;

use App\Entity\CompanyRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompanyRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyRate[]    findAll()
 * @method CompanyRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompanyRate::class);
    }

    // /**
    //  * @return CompanyRate[] Returns an array of CompanyRate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyRate
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
